<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $product = \App\Product::all();
    return view('welcome', ['product' => $product]);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/routes_api', 'HomeController@api_index')->name('routes_api');



Route::get('/user', 'UserController@index')->name('user');
Route::get('/create_user', 'UserController@create')->name('create_user');
Route::post('/creating_user', 'UserController@store')->name('creating_user');

Route::get('/category', 'CategoryController@index')->name('category');
Route::get('/create_category', 'CategoryController@create')->name('create_category');
Route::post('/creating_category', 'CategoryController@store')->name('creating_category');

Route::get('/product', 'ProductController@index')->name('product');
Route::get('/create_product', 'ProductController@create')->name('create_product');
Route::post('/creating_product', 'ProductController@store')->name('creating_product');

Route::get('/rate', 'RateController@index')->name('rate');
Route::post('/creating_rate', 'RateController@store')->name('creating_rate');



Route::get('/edit_user/{id}', 'UserController@edit')->name('edit_user');
Route::post('/editing_user/{id}', 'UserController@update')->name('editing_user');
Route::get('/delete_user/{id}', 'UserController@destroy')->name('delete_user');

Route::get('/buying_product/{id}', 'UserController@buying')->name('buying_product');

Route::get('/edit_category/{id}', 'CategoryController@edit')->name('edit_category');
Route::post('/editing_category/{id}', 'CategoryController@update')->name('editing_category');
Route::get('/delete_category/{id}', 'CategoryController@destroy')->name('delete_category');

Route::get('/edit_product/{id}', 'ProductController@edit')->name('edit_product');
Route::post('/editing_product/{id}', 'ProductController@update')->name('editing_product');
Route::get('/delete_product/{id}', 'ProductController@destroy')->name('delete_product');


Route::get('/create_rate/{id}', 'RateController@create')->name('create_rate');
Route::get('/edit_rate/{id}', 'RateController@edit')->name('edit_rate');
Route::post('/editing_rate/{id}', 'RateController@update')->name('editing_rate');
Route::get('/delete_rate/{id}', 'RateController@destroy')->name('delete_rate');


