<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('/login', 'UserApiController@login');

Route::middleware('auth:api')->get('/user', 'UserApiController@index');

Route::middleware('auth:api')->get('/category', 'CategoryApiController@index');
Route::middleware('auth:api')->post('/category_store', 'CategoryApiController@store');

Route::middleware('auth:api')->get('/products', 'ProductApiController@index');
Route::middleware('auth:api')->get('/product', 'ProductApiController@view_all_products');
Route::middleware('auth:api')->post('/product_store', 'ProductApiController@store');

Route::middleware('auth:api')->get('/category_show/{id}', 'CategoryApiController@show');
Route::middleware('auth:api')->post('/category_update/{id}', 'CategoryApiController@update');
Route::middleware('auth:api')->get('/category_delete/{id}', 'CategoryApiController@destroy');

Route::middleware('auth:api')->get('/product_show/{id}', 'ProductApiController@show');
Route::middleware('auth:api')->post('/product_update/{id}', 'ProductApiController@update');
Route::middleware('auth:api')->get('/product_delete/{id}', 'ProductApiController@destroy');

