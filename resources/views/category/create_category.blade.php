@extends('layouts.app')
@section('title', 'StudioGenesis')
@section('content')
<div class="container-fluid">
    <div class="row">
        @include('layouts.components.sidebar')

        <div class="col-md-10">
            <div class="row">
                @if(isset($status))
                    <div class="col-md-12 mt-3">
                        <div class="alert-{{$status}} p-2 mb-4">
                            <span>{{ $response }}</span>
                        </div>
                    </div>
                @endif
                <div class="col-md-12">
                    <form action="{{ url('creating_category') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="name">Nombre de la categoría *</label>
                            <input type="text" class="form-control" id="name" name="name" required>
                        </div>
                        <div class="form-group">
                            <label for="name">Descripción</label>
                            <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="float-right">
                                    <button type="submit" class="btn btn-primary mt-3">Crear nueva categoria</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
