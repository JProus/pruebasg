@extends('layouts.app')
@section('title', 'StudioGenesis')
@section('content')
<div class="container-fluid">
    <div class="row">
        @include('layouts.components.sidebar')

        <div class="col-md-10">
            <div class="row">
                <div class="col-md-12">
                    <div class="float-right">
                        <a href="{{ url('create_category') }}" class="btn btn-outline-primary">Crear nueva categoria</a>
                    </div>
                </div>
                <div class="col-md-12">
                    @if(count($category) === 0)
                        <div class="alert-warning mt-4 p-3 text-center">
                            <span><strong>Cuidado</strong> no hay ninguna categoría creada.</span>
                        </div>
                    @else
                        <table class="table table-hover mt-3">
                            <tr>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Opciones</th>
                            </tr>
                            @foreach($category as $c)
                                <tr>
                                    <td>
                                        {{ $c->name }}
                                    </td>
                                    <td>
                                        {{ $c->description }}
                                    </td>
                                    <td>
                                        <a href="{{ url('edit_category') }}/{{ $c->id }}" class="btn btn-warning mr-2">Editar</a>
                                        <a href="{{ url('delete_category') }}/{{ $c->id }}" class="btn btn-outline-danger">Eliminar</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
