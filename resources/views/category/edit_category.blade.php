@extends('layouts.app')
@section('title', 'StudioGenesis')
@section('content')
<div class="container-fluid">
    <div class="row">
        @include('layouts.components.sidebar')

        <div class="col-md-10">
            <div class="row">
                <div class="col-md-12 mb-3">
                    <a href="{{ url('home') }}">Home</a> / <a href="{{ url('category') }}">Lista de categorias</a> / <span class="text-muted">Edición de la categoría {{ $category->name }}</span>
                </div>
            </div>
            <div class="row">
                @if(isset($status))
                    <div class="col-md-12 mt-3">
                        <div class="alert-{{$status}} p-2 mb-4">
                            <span>{{ $response }}</span>
                        </div>
                    </div>
                @endif
                <div class="col-md-12">
                    <form action="{{ url('editing_category') }}/{{$category->id}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">Nombre de la categoría</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ $category->name }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="name">Descripción</label>
                                    <textarea class="form-control" id="description" name="description" rows="3">{{ $category->description }}</textarea>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="float-right">
                                            <button type="submit" class="btn btn-warning mt-3">Editar categoría</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
