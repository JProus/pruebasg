@extends('layouts.app')
@section('title', 'StudioGenesis')
@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('layouts.components.sidebar')

            <div class="col-md-10">
                <div class="col-md-12">
                    <h4 class="text-muted mt-3 mb-4">Redirecciones API</h4>
                    <div class="col-md-12">
                        <span>Ver datos de usuario:</span>
                        <br>
                        <a href="http://pruebatecnicasg.sytes.net/api/user?api_token={{ $user->api_token }}" target="_blank">http://pruebatecnicasg.sytes.net/api/user?api_token={{ $user->api_token }}</a>
                    </div>
                    <div class="col-md-12 mt-4">
                        <span class="mt-5">Ver categorias creadas:</span>
                        <br>
                        <a href="http://pruebatecnicasg.sytes.net/api/category?api_token={{ $user->api_token }}" target="_blank">http://pruebatecnicasg.sytes.net/api/category?api_token={{ $user->api_token }}</a>
                    </div>
                    <div class="col-md-12 mt-4">
                        <span class="mt-5">Ver producto con fecha:</span>
                        <br>
                        <a href="http://pruebatecnicasg.sytes.net/api/product?date=2019-06-25&api_token={{ $user->api_token }}" target="_blank">http://pruebatecnicasg.sytes.net/api/product?date=2019-06-25&api_token={{ $user->api_token }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
