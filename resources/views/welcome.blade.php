@extends('layouts.app')
@section('title', 'StudioGenesis')
@section('content')
    <div class="container-fluid">
        <div class="row">
            @if(count($product) != 0)
                <div class="col-md-12">
                    <h4 class="text-secondary">Listado de productos</h4>
                </div>
                @foreach($product as $p)
                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-header">
                                <img src="{{ asset('storage/img/products/') }}/{{ $p->photo }}" alt="" width="100%">
                            </div>
                            <div class="card-body text-center">
                                {{ $p->name }}
                                <small>{{ $p->price }}€</small>
                            </div>
                            @if(Auth::user())
                                <div class="card-footer text-center"><a href="#"
                                                                        class="btn btn-outline-primary">Comprar</a>
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@endsection
