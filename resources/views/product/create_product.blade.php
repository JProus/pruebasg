@extends('layouts.app')
@section('title', 'StudioGenesis')
@section('content')
<div class="container-fluid">
    <div class="row">
        @include('layouts.components.sidebar')

        <div class="col-md-10">
            <div class="row">
                @if(isset($status))
                    <div class="col-md-12 mt-3">
                        <div class="alert-{{$status}} p-2 mb-4">
                            <span>{{ $response }}</span>
                        </div>
                    </div>
                @endif
                <div class="col-md-12">
                    <form action="{{ url('creating_product') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="name">Nombre del producto *</label>
                            <input type="text" class="form-control" id="name" name="name" required>
                        </div>
                        <div class="form-group">
                            <label for="name">Descripción</label>
                            <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="price">Precio *</label>
                                    <input type="number" class="form-control" id="price" name="price" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="quantity">Cantidad *</label>
                                    <input type="number" class="form-control" id="quantity" name="quantity" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="category">Categoria *</label>
                            <select name="category" id="category" class="form-control">
                                <option value="">Escoge una opción</option>
                                @foreach($category as $c)
                                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="float-right">
                                    <button type="submit" class="btn btn-primary mt-3">Crear nuevo producto</button>
                                </div>
                                <div class="form-group">
                                    <label for="photo">Subir una foto del producto:</label>
                                    <input type="file" class="custom-file" id="photo" name="photo">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
