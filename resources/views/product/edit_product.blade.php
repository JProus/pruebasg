@extends('layouts.app')
@section('title', 'StudioGenesis')
@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('layouts.components.sidebar')

            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <a href="{{ url('home') }}">Home</a> / <a href="{{ url('product') }}">Lista de productos</a> /
                        <span class="text-muted">Edición de la categoría {{ $product->name }}</span>
                    </div>
                </div>
                <div class="row">
                    @if(isset($status))
                        <div class="col-md-12 mt-3">
                            <div class="alert-{{$status}} p-2 mb-4">
                                <span>{{ $response }}</span>
                            </div>
                        </div>
                    @endif
                    <div class="col-md-12">
                        <form action="{{ url('editing_product') }}/{{$product->id}}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-2">
                                    <img src="{{asset('storage/img/products')}}/{{ $product->photo }}" alt="" width="100%">
                                    <div class="form-group">
                                        <label for="photo">Modificar foto del producto:</label>
                                        <input type="file" class="custom-file" id="photo" name="photo">
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="name">Nombre del producto</label>
                                        <input type="text" class="form-control" id="name" name="name"
                                               value="{{ $product->name }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Descripción</label>
                                        <textarea class="form-control" id="description" name="description"
                                                  rows="3">{{ $product->description }}</textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="price">Precio *</label>
                                                <input type="number" class="form-control" id="price" name="price"
                                                       value="{{ $product->price }}" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="quantity">Cantidad *</label>
                                                <input type="number" class="form-control" id="quantity" name="quantity"
                                                       value="{{ $product->quantity }}" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="category">Categoria *</label>
                                        <select name="category" id="category" class="form-control">
                                            <option value="">Escoge una opción</option>
                                            @foreach($category as $c)
                                                <option value="{{ $c->id }}"
                                                @if($category_active == $c->id){{'selected'}}@endif
                                                >{{ $c->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="float-left">
                                                @if($product->rate_id === null)
                                                    <a href="{{ url('create_rate') }}/{{ $product->id }}" class="btn btn-outline-info mt-3">Crear tarifa especial a este producto</a>
                                                @else
                                                    <a href="{{ url('edit_rate') }}/{{ $product->rates->id }}" class="btn btn-outline-info mt-3">Modificar tarifa asociada a este producto</a>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="float-right">
                                                <button type="submit" class="btn btn-warning mt-3">Editar producto
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
