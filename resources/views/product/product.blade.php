@extends('layouts.app')
@section('title', 'StudioGenesis')
@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('layouts.components.sidebar')

            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-12">
                        <div class="float-right">
                            @if(count($category) === 0)
                                <a class="btn btn-outline-primary disabled">Crear nuevo
                                    producto</a>
                            @else
                                <a href="{{ url('create_product') }}" class="btn btn-outline-primary">Crear nuevo
                                    producto</a>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12">
                        @if(count($category) === 0)
                            <div class="alert-danger mt-4 p-3 text-center">
                                <span><strong>Cuidado</strong> no hay ninguna categoría creada.</span>
                            </div>
                        @endif
                        <table class="table table-hover mt-3">
                            <tr>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Precio</th>
                                <th>Cantidad</th>
                                <th>Descuento</th>
                                <th>Opciones</th>
                            </tr>
                            @foreach($product as $p)
                                <tr>
                                    <td>
                                        {{ $p->name }}
                                    </td>
                                    <td>
                                        {{ $p->description }}
                                    </td>
                                    <td>
                                        {{ $p->price }}€
                                    </td>
                                    <td>
                                        {{ $p->quantity }}
                                    </td>
                                    <td>
                                        @if($p->rate_id !== NULL)
                                            <a href="{{ url('edit_rate') }}/{{ $p->rates->id }}">Descuento</a>
                                        @else
                                            {{ 'NO' }}
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ url('edit_product') }}/{{ $p->id }}" class="btn btn-warning mr-2">Editar</a>
                                        <a href="{{ url('delete_product') }}/{{ $p->id }}"
                                           class="btn btn-outline-danger">Eliminar</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                            @if(count($product) === 0)
                                <div class="alert-warning mt-4 p-3 text-center">
                                    <span><strong>Atención</strong> No hay ningún producto creado.</span>
                                </div>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
