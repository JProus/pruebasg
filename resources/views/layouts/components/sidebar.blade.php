<div class="col-md-2">
    <ul class="list-group list-group-flush">
        <li class="list-group-item sidemenu">
            <a href="{{ url('user') }}">Usuarios</a>
        </li>
        <li class="list-group-item sidemenu">
            <a href="{{ url('category') }}">Categorias</a>
        </li>
        <li class="list-group-item sidemenu">
            <a href="{{ url('product') }}">Productos</a>
        </li>
        <li class="list-group-item sidemenu">
            <a href="{{ url('rate') }}">Tarifas especiales</a>
        </li>
        <li class="list-group-item sidemenu">
            <a href="{{ url('routes_api') }}">Direcciones API</a>
        </li>
    </ul>
</div>