@extends('layouts.app')
@section('title', 'StudioGenesis')
@section('content')
<div class="container-fluid">
    <div class="row">
        @include('layouts.components.sidebar')

        <div class="col-md-10">
            <div class="row">
                @if(isset($status))
                    <div class="col-md-12 mt-3">
                        <div class="alert-{{$status}} p-2 mb-4">
                            <span>{{ $response }}</span>
                        </div>
                    </div>
                @endif
                <div class="col-md-12">
                    <h4 class="text-muted mb-3">Tarifa especial para el producto {{ $product->name }}</h4>
                    <form action="{{ url('creating_rate') }}" method="post">
                        @csrf
                        <input type="hidden" name="product" id="product" value="{{ $product->id }}">
                        <div class="form-group">
                            <label for="price">Precio producto con tarifa *</label>
                            <input type="number" class="form-control" id="price" name="price" value="{{ $product->price }}" required>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="start_date">Fecha de inicio *</label>
                                    <input type="date" class="form-control" id="start_date" name="start_date" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="end_date">Fecha de finalización</label>
                                    <input type="date" class="form-control" id="end_date" name="end_date">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="float-right">
                                    <button type="submit" class="btn btn-primary mt-3">Crear nueva tarifa</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
