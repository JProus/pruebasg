@extends('layouts.app')
@section('title', 'StudioGenesis')
@section('content')
<div class="container-fluid">
    <div class="row">
        @include('layouts.components.sidebar')

        <div class="col-md-10">
            <div class="row">
                <div class="col-md-12">
                        <table class="table table-hover mt-3">
                            <tr>
                                <th>Nombre producto</th>
                                <th>Precio oferta</th>
                                <th>Fecha de inicio</th>
                                <th>Fecha finalización</th>
                                <th>Opciones</th>
                            </tr>
                            @foreach($rate as $r)
                                <tr>
                                    <td>
                                        @foreach($r->products->get() as $p)
                                            @if($p->id === $r->product_id)
                                                {{ $p->name }}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        {{ $r->price }}
                                    </td>
                                    <td>
                                        {{ $r->start_date }}
                                    </td>
                                    <td>
                                        @if($r->end_date !== null)
                                            {{ $r->end_date }}
                                        @else
                                            {{ 'Sin fecha' }}
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ url('edit_rate') }}/{{ $r->id }}" class="btn btn-warning mr-2">Editar</a>
                                        <a href="{{ url('delete_rate') }}/{{ $r->id }}" class="btn btn-outline-danger">Eliminar</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
