@extends('layouts.app')
@section('title', 'StudioGenesis')
@section('content')
<div class="container-fluid">
    <div class="row">
        @include('layouts.components.sidebar')

        <div class="col-md-10">
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header text-center">
                            <span><i class="fa fa-user-alt font-home"></i></span>
                        </div>
                        <div class="card-footer text-center">
                            <a href="{{ url('user') }}" class="">Gestionar usuarios</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header text-center">
                            <span><i class="fa fa-ruler-combined font-home"></i></span>
                        </div>
                        <div class="card-footer text-center">
                            <a href="{{ url('category') }}" class="">Gestionar Categorias</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header text-center">
                            <span><i class="fa fa-tshirt font-home"></i></span>
                        </div>
                        <div class="card-footer text-center">
                            <a href="{{ url('product') }}" class="">Gestionar Productos</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
