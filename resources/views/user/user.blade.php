@extends('layouts.app')
@section('title', 'StudioGenesis')
@section('content')
<div class="container-fluid">
    <div class="row">
        @include('layouts.components.sidebar')

        <div class="col-md-10">
            <div class="row">
                <div class="col-md-12">
                    <div class="float-right">
                        <a href="{{ url('create_user') }}" class="btn btn-outline-primary">Crear nuevo usuario</a>
                    </div>
                </div>
                <div class="col-md-12">
                    @if(count($user) === 0)
                        <div class="alert-warning mt-4 p-3 text-center">
                            <span><strong>Cuidado</strong> no hay ningún usuario creado.</span>
                        </div>
                    @else
                        <table class="table table-hover mt-3">
                            <tr>
                                <th>Nombre</th>
                                <th>Apellidos</th>
                                <th>Email</th>
                                <th>fecha de nacimiento</th>
                                <th>Opciones</th>
                            </tr>
                            @foreach($user as $u)
                                <tr>
                                    <td>
                                        {{ $u->name }}
                                    </td>
                                    <td>
                                        {{ $u->first_last_name }} {{ $u->second_last_name }}
                                    </td>
                                    <td>
                                        {{ $u->email }}
                                    </td>
                                    <td>
                                        {{ $u->date_of_birth }}
                                    </td>
                                    <td>
                                        <a href="{{ url('edit_user') }}/{{ $u->id }}" class="btn btn-warning mr-2">Editar</a>
                                        <a href="{{ url('delete_user') }}/{{ $u->id }}" class="btn btn-outline-danger">Eliminar</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
