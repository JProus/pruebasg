@extends('layouts.app')
@section('title', 'StudioGenesis')
@section('content')
<div class="container-fluid">
    <div class="row">
        @include('layouts.components.sidebar')

        <div class="col-md-10">
            <div class="row">
                @if(isset($status))
                    <div class="col-md-12 mt-3">
                        <div class="alert-{{$status}} p-2 mb-4">
                            <span>{{ $response }}</span>
                        </div>
                    </div>
                @endif
                <div class="col-md-12">
                    <form action="{{ url('creating_user') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="name">Nombre *</label>
                            <input type="text" class="form-control" id="name" name="name" required>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="first_last_name">Primer apellido</label>
                                    <input type="text" class="form-control" id="first_last_name" name="first_last_name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="second_last_name">Segundo apellido</label>
                                    <input type="text" class="form-control" id="second_last_name" name="second_last_name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="phone">Teléfono</label>
                                    <input type="text" class="form-control" id="phone" name="phone">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="date_of_birth">Fecha de nacimiento</label>
                                    <input type="date" class="form-control" id="date_of_birth" name="date_of_birth" placeholder="dd/mm/yyyy">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email">Correo electrónico *</label>
                            <input type="email" class="form-control" id="email" name="email" required>
                        </div>
                        <div class="form-group">
                            <label for="password">Contraseña *</label>
                            <input type="password" class="form-control" id="password" name="password" minlength="8" required>
                        </div>
                        <div class="form-group">
                            <label for="password1">Repetir Contraseña *</label>
                            <input type="password" class="form-control" id="password1" name="password1" minlength="8" required>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="float-right">
                                    <button type="submit" class="btn btn-primary mt-3">Crear nuevo usuario</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
