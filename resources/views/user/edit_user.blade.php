@extends('layouts.app')
@section('title', 'StudioGenesis')
@section('content')
<div class="container-fluid">
    <div class="row">
        @include('layouts.components.sidebar')

        <div class="col-md-10">
            <div class="row">
                <div class="col-md-12 mb-3">
                    <a href="{{ url('home') }}">Home</a> / <a href="{{ url('user') }}">Lista de usuarios</a> / <span class="text-muted">Edición de usuario {{ $user->name }}</span>
                </div>
            </div>
            <div class="row">
                @if(isset($status))
                    <div class="col-md-12 mt-3">
                        <div class="alert-{{$status}} p-2 mb-4">
                            <span>{{ $response }}</span>
                        </div>
                    </div>
                @endif
                <div class="col-md-12">
                    <form action="{{ url('editing_user') }}/{{$user->id}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-2">
                                <img src="{{ asset('storage/img/users/') }}/{{$user->photo}}" alt="" width="100%">
                                <div class="form-group mt-3">
                                    <label for="photo">Cambiar fotografía</label>
                                    <input type="file" class="custom-file" id="photo" name="photo">
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label for="name">Nombre</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}" required>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="first_last_name">Primer apellido</label>
                                            <input type="text" class="form-control" id="first_last_name" name="first_last_name" value="{{ $user->first_last_name }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="second_last_name">Segundo apellido</label>
                                            <input type="text" class="form-control" id="second_last_name" name="second_last_name" value="{{ $user->second_last_name }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="phone">Teléfono</label>
                                            <input type="text" class="form-control" id="phone" name="phone" value="{{ $user->phone }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="date_of_birth">Fecha de nacimiento</label>
                                            <input type="date" class="form-control" id="date_of_birth" name="date_of_birth" placeholder="dd/mm/yyyy" value="{{ $user->date_of_birth }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email">Correo electrónico *</label>
                                    <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="password">Contraseña *</label>
                                    <input type="password" class="form-control" id="password" name="password" minlength="8">
                                </div>
                                <div class="form-group">
                                    <label for="password1">Nueva Contraseña *</label>
                                    <input type="password" class="form-control" id="password1" name="password1" minlength="8">
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="float-right">
                                            <button type="submit" class="btn btn-warning mt-3">Editar usuario</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
