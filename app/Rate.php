<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $fillable = [
        'product_id',
        'price',
        'start_date',
        'end_date'
    ];

    public function products()
    {
        return $this->hasOne('App\Product');
    }
}
