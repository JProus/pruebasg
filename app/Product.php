<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'description',
        'photo',
        'price',
        'quantity',
        'rate_id'
    ];

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function rates()
    {
        return $this->hasOne('App\Rate');
    }
}
