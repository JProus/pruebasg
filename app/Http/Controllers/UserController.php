<?php

namespace App\Http\Controllers;

use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Illuminate\Support\Facades\File;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
        return View('user.user', ['user' => $user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('user.create_user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->password === $request->password1) {
            try {
                $user = DB::table('users')->where('email', $request->email)->first();
                if (!$user) {
                    User::create([
                        'name' => $request->name,
                        'first_last_name' => ($request->first_last_name !== NULL) ? $request->first_last_name : NULL,
                        'second_last_name' => ($request->second_last_name !== NULL) ? $request->second_last_name : NULL,
                        'phone' => ($request->phone !== NULL) ? $request->phone : NULL,
                        'date_of_birth' => ($request->date_of_birth !== NULL) ? $request->date_of_birth : NULL,
                        'email' => $request->email,
                        'password' => bcrypt($request->password),
                        'photo' => 'imagen-no-disponible.gif',
                        'api_token' => Str::random(60),
                    ]);
                    return View('user.create_user',
                        [
                            'status' => 'success',
                            'response' => 'usuario creado correctamente'
                        ]);
                } else return View('user.create_user',
                    [
                        'status' => 'danger',
                        'response' => 'El usuario ya existe, prueba otras credenciales'
                    ]);
            } catch (Exception $e) {
                return View('user.create_user',
                    [
                        'status' => 'danger',
                        'response' => 'Ocurrió algún problema, consulta con el administrador'
                    ]);
            }
        }
        else {
            return View('user.create_user',
                [
                    'status' => 'danger',
                    'response' => 'Las contraseñas no coinciden'
                ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit($user)
    {
        $user = User::find($user);

        if (!$user) {
            return redirect()->route('user');
        }
        return View('user.edit_user', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user_id)
    {

        $user = User::find($user_id);
        $foto = "imagen-no-disponible.gif";

        if (Hash::check($request->password, $user->password)) {
            $pass = bcrypt($request->password1);
        } elseif ($request->password == NULL) {
            $pass = $user->password;
        } else {
            return View('user.edit_user', [
                'user' => $user,
                'status' => 'danger',
                'response' => 'No se ha podido editar el usuario, revisa que los campos sean correctos'
            ]);
        }

        if ($user->photo !== $foto) {
            $foto = $user->photo;
        }

        if ($request->photo !== NULL) {
            $file = $request->photo;
            $mimetype = File::mimeType($file);

            if (strpos($mimetype, "jpg")) $filename = $request->name . "-" . $request->id . ".jpg";
            else if (strpos($mimetype, "jpeg")) $filename = $request->name . "-" . $request->id . ".jpeg";
            else if (strpos($mimetype, "png")) $filename = $request->name . "-" . $request->id . ".png";

            if ($file) {
                Storage::disk('local')->put("/public/img/users/" . $filename, File::get($file));
            }

            $foto = $filename;
        }

        $user->name = $request->name;
        $user->first_last_name = ($request->first_last_name !== NULL) ? $request->first_last_name : NULL;
        $user->second_last_name = ($request->second_last_name !== NULL) ? $request->second_last_name : NULL;
        $user->phone = ($request->phone !== NULL) ? $request->phone : NULL;
        $user->date_of_birth = ($request->date_of_birth !== NULL) ? $request->date_of_birth : NULL;
        $user->email = $request->email;
        $user->password = $pass;
        $user->photo = $foto;

        $user->save();

        return View('user.edit_user', [
            'user' => $user,
            'status' => 'success',
            'response' => 'Usuario modificado correctamente'
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id)
    {
        try {
            User::destroy($user_id);
            return redirect()->route('user');

        } catch (\Exception $e) {
            return View('user.user',
                [
                    'status' => 'danger',
                    'response' => 'No se ha podido eliminar el usuario, consulta con el administrador'
                ]);
        }
    }

    public function buying($product_id)
    {
        $product = Product::find($product_id);

    }
}
