<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use PhpParser\Node\Expr\Array_;
use Illuminate\Support\Str;
use Psy\Util\Json;

class UserApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return array
     */
    public function index()
    {
        $user = User::all();
        $array_user = [];
        foreach ($user as $item) {
            array_push($array_user, $item);
        }
        return $array_user;
    }

    public function login(Request $request)
    {

        if ($request['api_token'] != null) {
            $user = User::where('api_token', $request->api_token)->get();
        }
        else {
            $user = User::where('email', $request->email)->get();
            if ($user == null || Hash::check($request->password, $user[0]->password) == false ) {
                return Json::encode(['data'=> 'KO']);
            }
        }

        return Json::encode(['data' => $user]);
    }
}
