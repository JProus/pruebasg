<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $category = Category::all();
        $product = Product::all();

        return View('product.product', ['category' => $category, 'product' => $product]);
    }

    public function create()
    {
        $category = Category::all();
        return View('product.create_product', ['category' => $category]);
    }

    public function store(Request $request)
    {
        $product = new Product();

        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->rate_id = NULL;

        if ($request->photo !== NULL) {
            $file = $request->photo;

            $mimetype = File::mimeType($file);

            if (strpos($mimetype, "jpg")) $filename = $request->name . "-" . Str::random(10) . ".jpg";
            else if (strpos($mimetype, "jpeg")) $filename = $request->name . "-" . Str::random(10) . ".jpeg";
            else if (strpos($mimetype, "png")) $filename = $request->name . "-" . Str::random(10) . ".png";
            if ($file) {
                Storage::disk('local')->put("/public/img/products/" . $filename, File::get($file));
            }
            $product->photo = $filename;
        } else {
            $product->photo = 'imagen-no-disponible.gif';
        }

        try {
            if ($product->save()) $product->categories()->sync($request->category);
            return redirect()->route('product',
                ['status' => 'success', 'response' => 'Producto creado correctamente']);
        } catch (\Exception $e) {
            return redirect()->route('product',
                ['status' => 'danger',
                    'response' => 'Problema al crear el producto']);
        }
    }

    public function edit($product_id)
    {
        $product = Product::find($product_id);
        $category = Category::all();
        $category_active = $product->categories()->where('product_id', '=', $product_id)->get();

        return View('product.edit_product', ['product' => $product, 'category' => $category, 'category_active' => $category_active[0]->id]);
    }

    public function update(Request $request, $product_id)
    {
        $product = Product::find($product_id);

        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->rate_id = NULL;

        if ($request->photo !== NULL) {
            $file = $request->photo;

            $mimetype = File::mimeType($file);

            if (strpos($mimetype, "jpg")) $filename = $request->name . "-" . Str::random(10) . ".jpg";
            else if (strpos($mimetype, "jpeg")) $filename = $request->name . "-" . Str::random(10) . ".jpeg";
            else if (strpos($mimetype, "png")) $filename = $request->name . "-" . Str::random(10) . ".png";
            if ($file) {
                Storage::disk('local')->put("/public/img/products/" . $filename, File::get($file));
            }
            $product->photo = $filename;
        }

        try {
            if ($product->save()) $product->categories()->sync($request->category);
            return redirect()->route('product',
                ['status' => 'success', 'response' => 'Producto modificado correctamente']);
        } catch (\Exception $e) {
            return redirect()->route('product',
                ['status' => 'danger',
                    'response' => 'Problema al modificar el producto']);
        }

    }


    public function destroy($product_id)
    {
        try {
            $product = Product::find($product_id);
            $product->categories()->detach();
            Product::destroy($product_id);
            return redirect()->route('product');
        } catch (\Exception $e) {
            return redirect()->route('product');
        }
    }


}
