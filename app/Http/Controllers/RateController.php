<?php

namespace App\Http\Controllers;

use App\Product;
use App\Rate;
use Illuminate\Http\Request;

class RateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $rate = Rate::all();

        return View('rate.rate', ['rate' => $rate]);
    }

    public function create($id_product)
    {
        $product = Product::find($id_product);
        if (!$product) {
            return redirect()->route('rate');
        }
        return View('rate.create_rate', ['product' => $product]);
    }

    public function store(Request $request)
    {
        $product = Product::find($request->product);

        $rate = new Rate();

        $rate->product_id = $request->product;
        $rate->price = $request->price;
        $rate->start_date = $request->start_date;
        $rate->end_date = $request->end_date;

        try{
            if ($rate->save()) {
                $product->rate_id = $rate->id;
                $product->save();
            }
            return redirect()->route('edit_product', ['product' => $product]);
        }catch (\Exception $e) {
            return redirect()->route('edit_product', ['product' => $product]);
        }
    }

    public function edit($rate_id)
    {
        $rate = Rate::find($rate_id);
        $product = $rate->products()->where('rate_id', $rate_id)->get();
        return View('rate.edit_rate', ['rate' => $rate, 'product' => $product[0]]);
    }

    public function update(Request $request, $rate_id)
    {
        $rate = Rate::find($rate_id);

        $rate->price = $request->price;
        $rate->start_date = $request->start_date;
        $rate->end_date = $request->end_date;

        try{
            $rate->save();
            return redirect()->route('rate', ['status' => 'success',
                'response' => 'Tarifa modificada correctamente']);
        }catch (\Exception $e) {
            return redirect()->route('rate',['status' => 'danger',
                    'response' => 'Problema al modificar la tarifa']);
        }
    }

    public function destroy($rate_id)
    {
        try{
            $rate = Rate::find($rate_id);

            foreach ($rate->products->get() as $item) {
                if($item->rate_id == $rate_id){
                    $product = $item;
                    $product->rate_id = null;
                    $product->save();
                }
            }
            $rate->delete();
            return redirect()->route('rate');
        }catch (\Exception $e) {
            return redirect()->route('rate');
        }
    }
}
