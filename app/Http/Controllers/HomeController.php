<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = User::all()->count();
        $products = Product::all();
        $categories = Category::all();
        return view('home',
            [   'users'         => $users,
                'products'      => $products,
                'categories'    => $categories
            ]
        );
    }

    public function api_index()
    {
        return View('apiview', ['user' => Auth::user()]);
    }
}
