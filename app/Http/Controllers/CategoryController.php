<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $category = Category::all();
        return View('category.category', ['category' => $category]);
    }

    public function create()
    {
        return View('category.create_category');
    }

    public function store(Request $request)
    {
        try {
            Category::create($request->all());
            return View('category.create_category',
                [
                    'status' => 'success',
                    'response' => 'Categoría creada correctamente'
                ]);
        } catch (\Exception $e) {
            return View('category.create_category',
                [
                    'status' => 'danger',
                    'response' => 'Error al crear la categoría.'
                ]);
        }
    }

    public function edit($category_id)
    {
        $category = Category::find($category_id);

        if (!$category) {
            return redirect()->route('category');
        }

        return View('category.edit_category', ['category' => $category]);
    }


    public function update(Request $request, $category_id)
    {
        $category = Category::find($category_id);

        if (!$category) return redirect()->route('category');

        try {
            $category->name = $request->name;
            $category->description = $request->description;

            $category->save();
            return View('category.edit_category', [
                'category' => $category,
                'status' => 'success',
                'response' => 'Categoría modificada correctamente'
            ]);
        } catch (\Exception $e) {
            return View('category.edit_category', [
                'category' => $category,
                'status' => 'danger',
                'response' => 'Error al modificar la categoría'
            ]);
        }

    }

    public function destroy($category_id)
    {
        try {
            Category::destroy($category_id);
            return redirect()->route('category');
        } catch (\Exception $e) {
            return redirect()->route('category');
        }
    }
}
