<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Product::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new Product();

        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->rate_id = NULL;
        $product->photo = 'imagen-no-disponible.gif';
        try {
            if ($product->save()) $product->categories()->sync($request->category);
            return response()->json(['status' => 'OK']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'KO']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function view_all_products(Request $request)
    {
        $product = Product::all();

        $products = [];

        foreach ($product as $p) {
            if ($p->rate_id === null) {
                array_push($products, $p);
            }
            else {

                $price = ($request->date > $p->rates->start_date)?$p->rates->price:$p->price;
                if($p->rates->end_date !== null){
                    $price = ($request->date > $p->rates->end_date)?$p->price:$p->rates->price;
                }

                array_push($products, array(
                    'id'=> $p->id,
                    'name' => $p->name,
                    'description' => $p->description,
                    'photo' => $p->photo,
                    'price' => $price,
                    'quantity'  => $p->quantity,
                    'rate_id'   => $p->rate_id,
                    'created_at'    => $p->created_at,
                    'updated_at'    => $p->updated_at
                ));
            }
        }

        return $products;

    }

    public function show($product_id)
    {
        $category = Product::find($product_id);

        if ($category !== null) {
            return $category;
        }
        return response()->json(['status' => 'KO']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $product_id)
    {
        $product = Product::find($product_id);

        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->quantity = $request->quantity;

        try {
            if ($product->save()) $product->categories()->sync($request->category);
            return response()->json(['status' => 'OK']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'KO']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($product_id)
    {
        try {
            $product = Product::find($product_id);
            $product->categories()->detach();
            Product::destroy($product_id);
            return response()->json(['status' => 'OK']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'KO']);
        }
    }
}
